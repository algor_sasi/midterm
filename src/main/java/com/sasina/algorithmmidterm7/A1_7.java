/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.algorithmmidterm7;

import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author admin
 */
public class A1_7 {
    public static void main(String[] args) {
//        int c = 5;
//        int[] A = {2, 3, 8, 5};
    Scanner input = new Scanner(new InputStreamReader(System.in));
        int c = input.nextInt();
        int n = input.nextInt();
        int A[] = new int[n];
        
        for (int i=0; i<n; i++){
            A[i] = input.nextInt();
        }
        
        System.out.println(isSumEqualC(c, A));
    }
    
    public static String isSumEqualC(int c, int[] A) {
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (c == A[i] + A[j] && 1<= A[i] && A[i] < A[j] && A[j] <= A.length) {
                    return "Sum of " + A[i] + " , " + A[j] + " is equal " + c;
                }
            }
        }
        return "Is not have sum that equal c";
    }
}
