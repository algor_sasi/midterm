/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.algorithmmidterm8;

import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author admin
 */
public class A1_8 {
    public static void main(String[] args) {
//        int[] A = {3,4,1,5};
        Scanner input = new Scanner(new InputStreamReader(System.in));
        int n = input.nextInt();
        int A[] = new int[n];
        
        for (int i=0; i<n; i++){
            A[i] = input.nextInt();
        }
        
        reverseArray(A);
        
        for (int i=0; i< A.length; i++) {
            System.out.print(A[i] + " ");
        }
    }
    
    public static int[] reverseArray(int[] A) {
        int start = 0;
        int end = A.length - 1;
        
        while (start < end) {
            int temp = A[start];
            A[start] = A[end];
            A[end] = temp;
            
            start = start + 1;
            end = end - 1;
        }
        return A;
    }
}
